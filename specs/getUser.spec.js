var request = require('supertest')(process.env.NODE_ENV);
var expect = require('chai').expect;



describe("Access API endpoints", function () {

    it("Should Check out", function (done) {
        request
            .get('/access/:id')
            .set('Content-type', 'application/json')
            .auth('alex@tms.com', 'lSaOXK7PhxTnliN6')
            .expect('Content-Type', /json/)//Verify that a valid JSON object is returned 
            .expect(200)
        done();
    });// Test case to verify that a PAP can be checked out 

    it("Should Check in", function (done) {
        request
            .put('/access/:id')
            .set('Content-type', 'application/json')
            .auth('alex@tms.com', 'lSaOXK7PhxTnliN6')
            .expect('Content-Type', /json/)//Verify that a valid JSON object is returned 
            .expect(200)
        done();
    });// Test case to verify that a PAP can be checked in


    


});