## instructions on how to setup and run test cases.

1. Ensure that you are in the API testing folder. 

2. Run the npm install command.

       npm install

3. Run Single Test

       npm run single 'location of test file' 
       eg. npm run single specs/users.spec.js 
      
4. Run Test Suite 

       npm test
